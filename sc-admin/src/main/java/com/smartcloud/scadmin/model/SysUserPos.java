package com.smartcloud.scadmin.model;

public class SysUserPos {
    private String id;

    private String posId;

    private String userId;

    private String isPrimaryPos;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPosId() {
        return posId;
    }

    public void setPosId(String posId) {
        this.posId = posId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getIsPrimaryPos() {
        return isPrimaryPos;
    }

    public void setIsPrimaryPos(String isPrimaryPos) {
        this.isPrimaryPos = isPrimaryPos;
    }
}