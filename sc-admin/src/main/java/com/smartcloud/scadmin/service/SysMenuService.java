package com.smartcloud.scadmin.service;

import com.github.pagehelper.PageHelper;
import com.smartcloud.scadmin.dao.SysMenuMapper;
import com.smartcloud.scadmin.model.SysMenu;
import com.smartcloud.scadmin.model.SysMenuExample;
import com.smartcloud.scadmin.model.TreeNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class SysMenuService {
	
	@Autowired
	private SysMenuMapper sysMenuMapper;
	
	public int insert(SysMenu sysMenu){
		return sysMenuMapper.insert(sysMenu);
	}
	
	public int updateByPrimaryKeySelective(SysMenu sysMenu){
		return sysMenuMapper.updateByPrimaryKeySelective(sysMenu);
	}
	
	public int updateByExample(SysMenu sysMenu, SysMenuExample example){
		return sysMenuMapper.updateByExample(sysMenu, example);
	}

	public int deleteByPrimaryKey(String id){
		return sysMenuMapper.deleteByPrimaryKey(id);
	}
	
	public int deleteBy(SysMenuExample example){
		return sysMenuMapper.deleteByExample(example);
	}
	
	public SysMenu selectByPrimaryKey(String id){
		return sysMenuMapper.selectByPrimaryKey(id);
	}
	
	public List<SysMenu> select(SysMenu sysMenu, int pageNo, int pageSize){
		PageHelper.startPage(pageNo, pageSize);
		PageHelper.orderBy("show_index asc");
		return sysMenuMapper.select(sysMenu);
	}
	
	public List<SysMenu> select(SysMenuExample example, int pageNo, int pageSize){
		PageHelper.startPage(pageNo, pageSize);
		PageHelper.orderBy("show_index asc");
		return sysMenuMapper.selectByExample(example);
	}
	
	public long count(SysMenu sysMenu){
		return sysMenuMapper.selectCount(sysMenu);
	}
	
	public List<TreeNode> selectTreeData(String parentId){
		List<SysMenu> list = sysMenuMapper.selectTreeData(parentId);
		return buildtree(list, parentId);
	}
	
	/* 
     * 以对象形式传回前台 
     */  
    public static List<TreeNode> buildtree(List<SysMenu> list, String id){
        List<TreeNode> treeNodes=new ArrayList<TreeNode>();  
        for (SysMenu sysMenu : list) {
            TreeNode node=new TreeNode();  
            node.setId(sysMenu.getId());  
            node.setLabel(sysMenu.getName());  
            node.setPath(sysMenu.getFilePath());
            node.setPid(sysMenu.getParentId());
            node.setIconCls(sysMenu.getRemark());
            node.setShowIndex(sysMenu.getShowIndex());
            if(id.equals(sysMenu.getParentId())){  
                node.setChildren(buildtree(list, node.getId()));  
                //菜单子节点按照showindex进行排序
                int insertIndex = treeNodes.size();
                for(int i=0; i<treeNodes.size(); i++){
                	int tempIndex = treeNodes.get(i).getShowIndex() == null ? 0: treeNodes.get(i).getShowIndex();
                	int nodeShowIndex = node.getShowIndex() == null ? 0: node.getShowIndex();
            		if(nodeShowIndex <= tempIndex ){
            			insertIndex = i;
            			break;
                	}
                }
                treeNodes.add(insertIndex, node);  
            }  
              
        }  
        return treeNodes;  
    }  
}
