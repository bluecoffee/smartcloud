package com.smartcloud.scadmin.dao;


import com.smartcloud.scadmin.model.SysMenu;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

public interface SysMenuMapper extends Mapper<SysMenu>{
    List<SysMenu> selectTreeData(String parentId);
}