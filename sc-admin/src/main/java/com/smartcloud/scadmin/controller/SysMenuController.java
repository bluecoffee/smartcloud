package com.smartcloud.scadmin.controller;

import com.smartcloud.common.invoke.SearchModelInvoker;
import com.smartcloud.common.util.Guid;
import com.smartcloud.common.util.ResultMessage;
import com.smartcloud.scadmin.model.SysMenu;
import com.smartcloud.scadmin.model.SysMenuExample;
import com.smartcloud.scadmin.model.TreeNode;
import com.smartcloud.scadmin.service.SysMenuService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.lang.reflect.InvocationTargetException;
import java.util.Date;
import java.util.List;

@Api(value="菜单",tags={"菜单"})
@RestController
@RequestMapping("/menu")
public class SysMenuController {
	
	@Autowired
	private SysMenuService sysMenuService;

	@ApiOperation("新增")
	@PostMapping
	public ResultMessage insert(SysMenu sysMenu){
	
		sysMenu.setId(Guid.newGuid());
		sysMenu.setCreateTime(new Date());
		sysMenu.setUpdateTime(new Date());
//		sysMenu.setCreateUserId(SysUser.getCurrentUser().getId());
//		sysMenu.setCreateUserName(SysUser.getCurrentUser().getUserName());
		sysMenuService.insert(sysMenu);
		return new ResultMessage(ResultMessage.SUCCESS);
	}

	@ApiOperation("查询")
	@GetMapping("/")
	public ResultMessage list(SysMenu sysMenu, @RequestParam("pageNo") int pageNo,@RequestParam("pageSize") int pageSize){
		List<SysMenu> list = sysMenuService.select(sysMenu, pageNo, pageSize);
		int totalCount = (int) sysMenuService.count(sysMenu);
		return new ResultMessage(ResultMessage.SUCCESS, totalCount, list);
	}

	@ApiOperation("根据条件查询")
	@GetMapping("/listByCondition")
	public ResultMessage listByCondition(SysMenu sysMenu, @RequestParam("pageNo") int pageNo,@RequestParam("pageSize") int pageSize){
		SysMenuExample example = new SysMenuExample();
		try {
			if(sysMenu != null){
				example = (SysMenuExample) SearchModelInvoker.invoke(sysMenu, example);
//				example.createCriteria().andNameLike(sysMenu.getName()).andCreateUserNameLike(sysMenu.getCreateUserName());
			}
		} catch (IllegalAccessException e) {
			e.printStackTrace();
			return new ResultMessage(ResultMessage.ERROR, e.getMessage());
		} catch (InvocationTargetException e) {
			e.printStackTrace();
			return new ResultMessage(ResultMessage.ERROR, e.getMessage());
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
			return new ResultMessage(ResultMessage.ERROR, e.getMessage());
		}
		List<SysMenu> list = sysMenuService.select(example, pageNo, pageSize);
		int totalCount = (int) sysMenuService.count(sysMenu);
		return new ResultMessage(ResultMessage.SUCCESS, totalCount, list);
	}

	@ApiOperation("修改")
	@PutMapping
	public ResultMessage update(SysMenu sysMenu){
		int rows = sysMenuService.updateByPrimaryKeySelective(sysMenu);
		if(rows > 0){
			return new ResultMessage(ResultMessage.SUCCESS);
		}else{
			return new ResultMessage(ResultMessage.ERROR, "更新失败！");
		}
		
	}

	@ApiOperation("删除")
	@DeleteMapping
	public ResultMessage delete(SysMenu sysMenu){
		int rows = 0;
			rows = sysMenuService.deleteByPrimaryKey(sysMenu.getId());
		if(rows > 0){
			return new ResultMessage(ResultMessage.SUCCESS);
		}else{
			return new ResultMessage(ResultMessage.ERROR, "删除失败！");
		}
	}

	@ApiOperation("查询树型数据")
	@GetMapping("/treeData")
	public ResultMessage selectTreeData(String parentId){
		List<TreeNode> treeNodeList = sysMenuService.selectTreeData(parentId);
		return new ResultMessage(ResultMessage.SUCCESS, treeNodeList);
	}

	@ApiOperation("根据id查询")
	@GetMapping("/{id}")
	public ResultMessage getOneByKey(@PathVariable("id") String id){
		return new ResultMessage(ResultMessage.SUCCESS, sysMenuService.selectByPrimaryKey(id));
	}
}
