package com.smartcloud.scadmintable.controller;

import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import com.smartcloud.common.util.Guid;
import com.smartcloud.common.util.ResultMessage;
import com.smartcloud.scadmintable.model.SysTable;
import com.smartcloud.scadmintable.service.SysTableService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import com.github.pagehelper.Page;

@Api(value="数据库表",tags={"数据库表"})
@RestController
@RequestMapping("/table")
public class SysTableController{
	
	@Autowired
	private SysTableService sysTableService;

	@ApiOperation("新增")
	@PostMapping
	public ResultMessage insert(@RequestBody SysTable sysTable){
	
		sysTable.setId(Guid.newGuid());
		sysTable.setCreateTime(new Date());
		sysTable.setUpdateTime(new Date());
		try {
			sysTableService.insert(sysTable);
		} catch (SQLException e) {
			e.printStackTrace();
			return new ResultMessage(ResultMessage.ERROR, e.getMessage());
		}
		return new ResultMessage(ResultMessage.SUCCESS);
	}

	@ApiOperation("查询")
	@GetMapping
	public ResultMessage list(SysTable sysTable, @RequestParam("pageNo") int pageNo,@RequestParam("pageSize") int pageSize){
		Page<SysTable> listPage = sysTableService.selectAllWithList(sysTable, pageNo, pageSize);
		return new ResultMessage(ResultMessage.SUCCESS, (int)listPage.getTotal(), listPage);
	}

	@ApiOperation("修改")
	@PutMapping
	public ResultMessage update(@RequestBody SysTable sysTable){
		int rows = 0;
		try {
			rows = sysTableService.updateByPrimaryKeySelective(sysTable);
		} catch (SQLException e) {
			e.printStackTrace();
			return new ResultMessage(ResultMessage.ERROR, e.getMessage());
		}
		if(rows > 0){
			return new ResultMessage(ResultMessage.SUCCESS);
		}else{
			return new ResultMessage(ResultMessage.ERROR, "更新失败！");
		}
		
	}

	@ApiOperation("删除")
	@DeleteMapping(value = "/{id}")
	public ResultMessage delete(@PathVariable("id") String id){
		int rows = 0;
		try {
			rows = sysTableService.deleteByPrimaryKey(id);
		} catch (SQLException e) {
			e.printStackTrace();
			return new ResultMessage(ResultMessage.ERROR, e.getMessage());
		}
		if(rows > 0){
			return new ResultMessage(ResultMessage.SUCCESS);
		}else{
			return new ResultMessage(ResultMessage.ERROR, "删除失败！");
		}
		 
	}

	@ApiOperation("批量删除")
	@DeleteMapping(value = "/muti/{ids}")
	public ResultMessage deleteMuti(@PathVariable("ids") String ids){
		String []idsArray = ids.split(",");
		if(idsArray.length > 0){
			int rows = 0;
			try {
				rows = sysTableService.deleteByPrimaryKeys(idsArray);
			} catch (SQLException e) {
				e.printStackTrace();
				return new ResultMessage(ResultMessage.ERROR, e.getMessage());
			}
			if(rows > 0){
				return new ResultMessage(ResultMessage.SUCCESS);
			}
		}
		return new ResultMessage(ResultMessage.ERROR, "删除失败！");
		
	}
}
