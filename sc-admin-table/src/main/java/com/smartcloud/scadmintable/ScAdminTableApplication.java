package com.smartcloud.scadmintable;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.web.bind.annotation.RestController;
import tk.mybatis.spring.annotation.MapperScan;

@SpringBootApplication
@EnableDiscoveryClient
@RestController
@MapperScan("com.smartcloud.scadmintable.dao")
public class ScAdminTableApplication {

	public static void main(String[] args) {
		SpringApplication.run(ScAdminTableApplication.class, args);
	}
}
