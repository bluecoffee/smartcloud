package com.smartcloud.scadmintable.model;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;

@Table(name = "sys_table")
public class SysTable {
	@Id
    private String id;

	@Column(name = "name")
    private String name;

    private String description;

    private String tableType;

    private String formType;

    private String pcStyle;

    private String mobileStyle;

    private String isPage;

    private String createUserId;

    private String createUserName;
    
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date createTime;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date updateTime;
    
    private List<SysTableColumn> sysTableColumnList;
    
    public List<SysTableColumn> getSysTableColumnList() {
		return sysTableColumnList;
	}

	public void setSysTableColumnList(List<SysTableColumn> sysTableColumnList) {
		this.sysTableColumnList = sysTableColumnList;
	}

	public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTableType() {
        return tableType;
    }

    public void setTableType(String tableType) {
        this.tableType = tableType;
    }

    public String getFormType() {
        return formType;
    }

    public void setFormType(String formType) {
        this.formType = formType;
    }

    public String getPcStyle() {
        return pcStyle;
    }

    public void setPcStyle(String pcStyle) {
        this.pcStyle = pcStyle;
    }

    public String getMobileStyle() {
        return mobileStyle;
    }

    public void setMobileStyle(String mobileStyle) {
        this.mobileStyle = mobileStyle;
    }

    public String getIsPage() {
        return isPage;
    }

    public void setIsPage(String isPage) {
        this.isPage = isPage;
    }

    public String getCreateUserId() {
        return createUserId;
    }

    public void setCreateUserId(String createUserId) {
        this.createUserId = createUserId;
    }

    public String getCreateUserName() {
        return createUserName;
    }

    public void setCreateUserName(String createUserName) {
        this.createUserName = createUserName;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
    
   
}